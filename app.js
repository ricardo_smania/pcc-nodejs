var application_root = __dirname,
    express = require("express"),
    path = require("path");

var app = express();

app.configure(function () {
	app.use(express.bodyParser());
	app.use(express.methodOverride());
	app.use(express.json());
	app.use(app.router);	
	app.use(express.static(path.join(application_root, "public")));
app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
});

app.options('/pcc', function (req, res) {	
    res.setHeader('Access-Control-Allow-Origin', '*')
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, X-Prototype-Version, Content-Type, Accept')
    res.end()
});

app.post('/pcc', function (req, res) {
	console.log(req.body)
	CalculoPcc = require('./calculo_pcc')
    calculo = new CalculoPcc()
    res.setHeader('Access-Control-Allow-Origin', '*')
    //entrada = JSON.parse(req.body)
    retorno = calculo.calcular_json(req.body)
    res.end(JSON.stringify(retorno, undefined, 2))
});

app.set('views', __dirname + '/views');
app.engine('html', require('ejs').renderFile);

app.get('/teste', function (req, res)
{
    res.render('teste.html');
});

app.get('/js', function (req, res)
{
    res.render('teste.html');
});

app.listen(88);