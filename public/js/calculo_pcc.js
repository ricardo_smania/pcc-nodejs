function CalculoPcc() {}

function calcular(calculo)
{
    retorno = []
    acumulado_meses = {}

    documentos_ordenados = calculo.documentos.sort(function (a, b) {
        if (a.status == b.status)
        {
            return a.data - b.data
        }
        else if (b.status == ':baixado')
            return 1
        else        
            return -1
            
    })  

    documentos_ordenados.forEach(function(documento) {
        if (documento.status == ":baixado")            
            data = new Date(documento.data)
        else
            data = new Date(calculo.data_baixa)

        data = new Date(data.getTime() + data.getTimezoneOffset() * 1000 * 60)

        mes = new Date(data.getFullYear(), data.getMonth(), 1)
        acumulado_meses[mes] = acumulado_meses[mes] || {valor: 0, pis: 0, cofins: 0, csll: 0}
        acumulado = acumulado_meses[mes]
        acumulado['valor'] += documento.valor
        imposto_retorno = calcula_impostos(documento.aliquotas, acumulado, documento.valor, calculo.opcoes['valorminimo'])
        retorno_doc = {
            chave: documento.chave,
            impostos: imposto_retorno
        }       
        retorno.push(retorno_doc)
    })

    return retorno      
}

function valida_entrada()
{
    if (!calculo.data_baixa) throw "data_baixa não informado"
}

CalculoPcc.prototype.calcular_json = function(json)
{
    calculo.data_baixa = json['data_baixa']
    calculo.opcoes = json['opcoes']
    calculo.documentos = json['documentos']    
    return calcular(this)
}

function calcula_impostos(aliquotas, acumulado, valor, valorminimo)
{   
    imposto_retorno = {}
    deve_zerar = false
    for (aliquota in aliquotas) {

        imposto = aliquota
        acumulado[imposto] += valor * aliquotas[aliquota] / 100            
        if (acumulado['valor'] >= valorminimo)
        {
            imposto_retorno[imposto] = acumulado[imposto]
            deve_zerar = true
        }
        else
            imposto_retorno[imposto] = 0

    }

    imposto_retorno = aplica_retencao_valor_maximo_documento(imposto_retorno, valor, acumulado, deve_zerar)

    imposto_retorno = {pis: Math.round(imposto_retorno['pis'] * 100) / 100,
                       cofins: Math.round(imposto_retorno['cofins'] * 100) / 100,
                       csll: Math.round(imposto_retorno['csll'] * 100) / 100 }

    return imposto_retorno
}


function aplica_retencao_valor_maximo_documento(imposto_retorno, valor, acumulado, deve_zerar)
{
    total_impostos = imposto_retorno['pis'] + imposto_retorno['cofins'] + imposto_retorno['csll']

    for (imposto in imposto_retorno) {
        if (total_impostos > valor)
        {
            valor_imposto = imposto_retorno[imposto] / total_impostos * valor
            diferenca = imposto_retorno[imposto] - valor_imposto
            acumulado[imposto] = diferenca
            imposto_retorno[imposto] = valor_imposto
        }
        else if (deve_zerar)
            acumulado[imposto] = 0
    }
    return imposto_retorno
}

try
{   
    module.exports = CalculoPcc
}
catch(err)
{

}