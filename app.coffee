application_root = __dirname
express = require("express")
path = require("path")
app = express()

# Config
app.configure ->
  app.use express.bodyParser()
  app.use express.methodOverride()
  app.use app.router
  app.use express.static(path.join(application_root, "public"))
  app.use express.errorHandler(
    dumpExceptions: true
    showStack: true
  )
  return

app.get "/api", (req, res) ->
  res.send req.query
  res.send "Ecomm API is runnin"
  return


# Launch server
app.listen 88
