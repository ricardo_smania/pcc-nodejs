DECLARE
  l_param_list     VARCHAR2(32000);
 
  l_http_request   UTL_HTTP.req;
  l_http_response  UTL_HTTP.resp;
 
  l_response_text  VARCHAR2(32767);
BEGIN
 
  -- service's input parameters
  l_param_list := '{
  "data_baixa": "2014-01-01",
  "opcoes": {
    "valorminimo": 5000
  },
  "documentos": [
    {
      "chave": 1,
      "data": "2014-01-01",
      "valor": 4990,
      "status": ":baixado",
      "aliquotas": {
        "pis": 0.65,
        "cofins": 3,
        "csll": 1
      },
      "retido": {
        "pis": 0,
        "cofins": 0,
        "csll": 0
      }
    },
    {
      "chave": 2,
      "valor": 10,
      "data": "2014-01-02",
      "status": ":baixado",
      "aliquotas": {
        "pis": 0.65,
        "cofins": 3,
        "csll": 1
      },
      "retido": {
        "pis": 1.4,
        "cofins": 6.45,
        "csll": 2.15
      }
    },
    {
      "chave": 3,
      "valor": 1000,
      "data": "2014-01-02",
      "aliquotas": {
        "pis": 0.65,
        "cofins": 3,
        "csll": 1
      }
    }
  ],
  ":valorminimo": 5000
}';
 
  -- preparing Request...
  l_http_request := UTL_HTTP.begin_request ('10.0.3.116:88/pcc'
                                          , 'POST'
                                          , 'HTTP/1.1');
 
  -- ...set header's attributes
  UTL_HTTP.set_header(l_http_request, 'Content-Type', 'application/json');
  UTL_HTTP.set_header(l_http_request, 'Content-Length', LENGTH(l_param_list));
 
  -- ...set input parameters
  UTL_HTTP.write_text(l_http_request, l_param_list);
 
  -- get Response and obtain received value
  l_http_response := UTL_HTTP.get_response(l_http_request);
 
  UTL_HTTP.read_text(l_http_response, l_response_text);
 
  DBMS_OUTPUT.put_line(l_response_text);
 
  -- finalizing
  UTL_HTTP.end_response(l_http_response);
 
EXCEPTION
  WHEN UTL_HTTP.end_of_body 
    THEN UTL_HTTP.end_response(l_http_response);  
END;
