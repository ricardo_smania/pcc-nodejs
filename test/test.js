assert = require("assert")

yaml = require('js-yaml');
fs   = require('fs');
CalculoPcc = require('../calculo_pcc')

describe('pcc', function(){
    var testes = yaml.safeLoad(fs.readFileSync('test/calc.yml', 'utf8'));

    testes.forEach(function(teste)
    {
        it(teste[':descricao'], function(){
            console.log("\n\n" + teste[':descricao']);
            ct = teste[':calculo']
            calculo = new CalculoPcc();            
            ct['opcoes'] = {valorminimo: 5000}
            retorno = calculo.calcular_json(ct);
            console.log(JSON.stringify(calculo, undefined, 2))
            esperado = teste[':retorno'];
            achou = false;            
            esperado.forEach(function(esperado)
            {                
                try
                {
                    retorno.forEach(function(valor)
                    {    
                        if (valor['chave'] == esperado[':chave'])
                        {
                            achou = true;
                            assert.equal(JSON.stringify(esperado[':impostos']), JSON.stringify(valor['impostos']));
                        }
                    });
                    assert.equal(achou, true, 'não encontrou documento');
                }
                catch(err)
                {
                    err.message = 'Documento ' + esperado[':chave'] + "\n     " + err.message;
                    err.stack = ""
                    throw err;
                }
            });

        })

    })
})